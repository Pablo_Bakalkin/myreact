import { NavLink } from 'react-router-dom'
import profilePostsReducer from '../../myRedux/profile-reducer'
import myStyle from './Header.module.css'

const Header = (props) => {
  
    let logoutFromAccount = () => {
      props.setLogout();
    }

    return  <header className={myStyle.header}>
    <img src='https://seeklogo.com/images/U/umbrella-corporation-logo-59C799E7A5-seeklogo.com.png'></img>
    <div className={myStyle.loginBlock}>
      {
        props.isAuth 
        ? <div>
          <p>{props.login}</p>
          <p>{props.email}</p>
          <button onClick={logoutFromAccount}>Logout</button>
        </div>
        : <NavLink to={'/login'}>Login</NavLink>
      }
    </div>
  </header>
}

export default Header
