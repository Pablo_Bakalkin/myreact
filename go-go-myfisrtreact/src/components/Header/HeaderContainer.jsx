import React from 'react';
import { connect } from "react-redux";
import Header from "./Header";
import { getLogin, setLogout} from "../../myRedux/auth-reducer";
import * as axios from 'axios'



class HeaderAPIComponent extends React.Component {
    componentDidMount(){
        this.props.getLogin();
        // getLoginAPI()
        // .then(data => {
        //     if(data.resultCode === 0){
        //         let {id, email, login} = data.data;
        //         this.props.setAuthUserData(id, email, login);
        //     }
        // })
    }

    render(){
        return <Header {...this.props}/> //props={...this.props}
    }
}

const mapStateToProps = (state) => {
    return{
        isAuth: state.auth.isAuth,
        id: state.auth.id,
        email: state.auth.email,
        login: state.auth.login
    }
}

let HeaderContainer = connect(mapStateToProps, {getLogin, setLogout})(HeaderAPIComponent);

export default HeaderContainer;