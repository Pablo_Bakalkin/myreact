import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import { setLogin } from '../../myRedux/auth-reducer';
import { maxLengthCreator, required } from '../../utils/validators';
import { createElementConstruct } from '../Common/FormsControls/FormsControls';
import styles from '../Common/FormsControls/FormsControls.module.css';

let maxLength35 = maxLengthCreator(35); 
let login =  createElementConstruct('input');
let password =  createElementConstruct('input');
let checkBox =  createElementConstruct('input');

const LoginForm = (props) => {
    return <form onSubmit={props.handleSubmit}>
        <div>
            <Field placeholder={'Login'} name={'login'} component={login} validate={[required, maxLength35]}/>
        </div>
        <div>
            <Field placeholder={'Password'} name={'password'} type='password' component={password} validate={[required, maxLength35]}/>
        </div>
        <div>
            <Field name={'rememberMe'} type={'checkBox'} component={checkBox}/> remember me
        </div>
        {
            props.error && <div className={styles.mainError}>{props.error}</div> 
        }
        <div>
            <button>Login</button>
        </div>
    </form>
}

const LoginReduxForm = reduxForm({form: 'login'}) (LoginForm)

const Login = (props) => {

    if(props.isAuth){
        return <Redirect to='/profile' />
    }
    

    const onSubmit = (formData) => {
        props.setLogin(formData)
        console.log(formData)
    }

    return <div>
        <h1>LOGIN</h1>
        <LoginReduxForm onSubmit={onSubmit}/>
    </div>
}

const mapStateToProps = (state) => {
    return{
        isAuth: state.auth.isAuth,
    }
}

export default connect(mapStateToProps, {setLogin})(Login);