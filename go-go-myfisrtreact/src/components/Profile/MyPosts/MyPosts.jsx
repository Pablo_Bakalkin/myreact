import { addPostActionCreator, writingNewPostActionCreator } from '../../../myRedux/profile-reducer';
import myStyle from './MyPosts.module.css'
import Post from './Posts/Post'
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { maxLengthCreator, required } from '../../../utils/validators';
import { createElementConstruct } from '../../Common/FormsControls/FormsControls';

let maxLength15 = maxLengthCreator(15); 
let postText = createElementConstruct('textarea')

const PostForm = (props) => {
    return <form onSubmit={props.handleSubmit}>
        <div>  
            <Field name='posttext' component={postText} validate={[required, maxLength15]}/>
        </div>
        <div>
            <button>Add post</button>
        </div>
    </form>
}

const LoginReduxForm = reduxForm({form: 'post'}) (PostForm)

const MyPosts = (props) => {
    
    let updatePosts = props.posts.map(post => <Post likes={post.likes} text={post.text}/>)

    //let newpost = React.createRef();

    // let addPost = () => {
    //     props.addPost();
    // }

    // let changePostText = () => {
    //     let text = newpost.current.value;
    //     props.changePostText(text);
    // } 

    let onAddPost = (values) => {
        props.addPost(values.posttext);
    }

    return (
        <div className={myStyle.tabPosts}>
            <h3>My posts</h3>
            <LoginReduxForm onSubmit={onAddPost}/>
            {/* <div>
                <textarea value={props.newPostText} onChange={changePostText} ref={newpost}></textarea>
            </div>
            <div>
                <button onClick={addPost}>Add post</button>
            </div> */}
            {updatePosts}
        </div>
    )
}

export default MyPosts;