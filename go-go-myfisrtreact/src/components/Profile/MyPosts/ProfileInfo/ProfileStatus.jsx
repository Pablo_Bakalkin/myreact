import React from 'react';

class ProfileStatus extends React.Component {
    state = {
        editMode: false,
        status: this.props.status
    }

    activateEditMode() {
        this.setState({
            editMode: true
        })
    }

    deactivateEditMode() {
        this.setState({
            editMode: false
        })
        this.props.updateStatus(this.state.status)
    }

    onStatusChange = (e) => {  
        this.setState({
            status: e.currentTarget.value
        })
    }

    componentDidUpdate(prevProp, prevState) {
        if(prevProp.status != this.props.status){
            this.setState({
                status: this.props.status
            })
        }
    }

    render(){
        return <>
            {
                !this.state.editMode && 
                <div>
                    <span onDoubleClick={this.activateEditMode.bind(this)}>{this.props.status || 'LOL NEMA STATUSA'}</span>
                </div>
            }
            {
                this.state.editMode && 
                <div>
                    <input autoFocus={true} onChange={this.onStatusChange} onBlur={this.deactivateEditMode.bind(this)} value={this.state.status} type="text"/>
                </div>
            }
        </>
    }
}

export default ProfileStatus;