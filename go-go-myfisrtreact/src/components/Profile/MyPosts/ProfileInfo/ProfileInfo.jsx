import Preloader from '../../../Common/Preloader/Preloader'
import myStyle from './ProfileInfo.module.css'
import savePhoto from '../../../../assets/images/241993457.jpg'
import ProfileStatusWithHooks from './ProfileStatusWithHooks'

const ProfileInfo = (props) => {
    if(!props.profile) return <Preloader/>

    let arr = []
    
    for (const key in props.profile.contacts) {
        if (Object.hasOwnProperty.call(props.profile.contacts, key)) {
            const element = props.profile.contacts[key];
            arr.push(props.profile.contacts[key]);
        }
    }

    return (
        <div>
            <div>
                <img src='https://p.bigstockphoto.com/GeFvQkBbSLaMdpKXF1Zv_bigstock-Aerial-View-Of-Blue-Lakes-And--227291596.jpg'></img>
            </div>
            <div className={myStyle.descriptionPadding}>
                <img src={props.profile.photos.large ? props.profile.photos.large : savePhoto} alt=""/>
                <div>
                    <p>{props.profile.fullName}</p>
                    <p>{props.profile.aboutMe}</p>
                    <p>{props.profile.lookingForAJobDescription}</p>
                    {arr.map(elm => {
                        return <p>{elm}</p>
                    })}
                </div>
                <ProfileStatusWithHooks status={props.status} updateStatus={props.updateStatus}/>
            </div>
        </div>
    )  
}

export default ProfileInfo