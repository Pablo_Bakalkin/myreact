import myStyle from './Post.module.css'

const Post = (props) => {
    return (
        <div className={myStyle.textColor}>
            <img src='https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431'></img>
            {props.text}
            <button> like {props.likes} </button>
        </div>
    )
}

export default Post