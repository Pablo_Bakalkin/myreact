import { addPostActionCreator, writingNewPostActionCreator } from '../../../myRedux/profile-reducer';
import myStyle from './MyPosts.module.css'
import Post from './Posts/Post'
import React from 'react';
import MyPosts from './MyPosts';
import { connect } from 'react-redux';



// const MyPostsContainer = (props) => {
//     let state = props.store.getState();

//     let addPost = () => {
//         props.store.dispatch(addPostActionCreator());
//     }

//     let changePostText = (text) => {
//         let action = writingNewPostActionCreator(text);
//         props.store.dispatch(action);
//     } 

//     return (
//         <MyPosts addPost={addPost} changePostText={changePostText} newPostText={state.myposts.addNewPost} posts={state.myposts.posts}/>
//     )
// }

const mapStateToProps = (state) => {
    return{
        //newPostText: state.myposts.addNewPost,
        posts: state.myposts.posts
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        addPost: (posttext) => {
            dispatch(addPostActionCreator(posttext))
        },
        // changePostText: (text) => {
        //     let action = writingNewPostActionCreator(text);
        //     dispatch(action);
        // }
    }
}

const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts);

export default MyPostsContainer;