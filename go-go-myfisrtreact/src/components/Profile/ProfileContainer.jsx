import React from 'react';
import { connect } from "react-redux";
import Profile from "./Profile";
import { getProfile, getStatus, updateStatus } from "../../myRedux/profile-reducer";
import { withRouter } from 'react-router-dom';
import { withAuthRedirectComponent } from '../../hoc/withAuthRedirect';
import { compose } from 'redux';



class ProfileAPIComponent extends React.Component{
    componentDidMount(){
        let userId = this.props.match.params.userId;

        if(this.props.isAuth && !userId) userId = this.props.profileId;
        this.props.getProfile(userId);
        this.props.getStatus(userId);

    }

    render(){
        return <Profile {...this.props} profile={this.props.profile} status={this.props.status} updateStatus={this.props.updateStatus}/>
    }
}

// let AuthRedirectComponent = withAuthRedirectComponent(ProfileAPIComponent);

const mapStateToProps = (state) => {
    return{
        profile: state.myposts.profile,
        status: state.myposts.status,
        profileId: state.auth.id,
        isAuth: state.auth.isAuth
    }
}



// let WithUrlDataContainerComponent = withRouter(AuthRedirectComponent);

// const ProfileContainer = connect(mapStateToProps, {getProfile})(WithUrlDataContainerComponent);

export default compose(
        connect(mapStateToProps, {getProfile, getStatus, updateStatus}),
        withRouter,
        withAuthRedirectComponent
)(ProfileAPIComponent);