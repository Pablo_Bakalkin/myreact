import { NavLink } from 'react-router-dom'
import BestFriends from './BestFriends/BestFriends'
import myStyle from './NavBar.module.css'

const Navbar = () => {
    return <nav className={myStyle.nav}>
    <div className={myStyle.item}>
      <NavLink to="/profile" activeClassName={myStyle.activeLink}>Profile</NavLink>
    </div>
    <div className={myStyle.item}>
      <NavLink to="/dialogs" activeClassName={myStyle.activeLink}>Dialogs</NavLink>
    </div>
    <div className={myStyle.item}>
      <NavLink to="/users" activeClassName={myStyle.activeLink}>Users</NavLink>
    </div>
    <div className={myStyle.item}>
      {/* <NavLink to="/news" activeClassName={myStyle.activeLink}>News</NavLink> */}
    </div>      
    <div className={myStyle.item}>
      {/* <NavLink to="/music" activeClassName={myStyle.activeLink}>Music</NavLink> */}
    </div>
    <div className={myStyle.item}>
      {/* <NavLink to="/settings" activeClassName={myStyle.activeLink}>Settings</NavLink> */}
    </div>

    <BestFriends />
  </nav>
}


export default Navbar