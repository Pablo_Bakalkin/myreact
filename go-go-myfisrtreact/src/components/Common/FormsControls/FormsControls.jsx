import React from 'react';
import styles from "./FormsControls.module.css";

export const createElementConstruct = (element) => {
    return ({input, meta, ...props}) => {
        const hasError = meta.error 
        return <div className={styles.formControl + ' ' + (hasError ? styles.error : '')}>
            <div>
                {React.createElement(element, {...input, ...props})}
            </div>
            {hasError && <span>{meta.error}</span>}
        </div>
    }
}
