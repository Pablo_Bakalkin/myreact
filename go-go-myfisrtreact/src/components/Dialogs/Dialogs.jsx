import { NavLink } from 'react-router-dom'
import myStyle from './Dialogs.module.css'
import React from 'react';
import { addMessageActionCreator, writingNewMessageActionCreator } from '../../myRedux/diologs-reducer';
import { Field, reduxForm } from 'redux-form';
import { createElementConstruct } from '../Common/FormsControls/FormsControls';
import { maxLengthCreator, required } from '../../utils/validators';

const DialogItem = (props) => {
    return(
        <div>
            <img src='https://steamuserimages-a.akamaihd.net/ugc/80345569855651747/FA7CEA28A702E4BE68B98ADD28082C3757416315/'></img>
            <NavLink to={`/dialogs/${props.id}`}>{props.name}</NavLink>
        </div>
    )
}

const Message = (props) => {
    return (
        <div id={props.id} className={props.className}>{props.message}</div>
    )
}

function letstry(text, id){
    if(id % 2 !== 0){
        return <Message message={text} id={id} className={myStyle.correctText}/>
    }else{
        return <Message message={text} id={id}/>
    }
}

let message = createElementConstruct('textarea')
let maxLength15 = maxLengthCreator(15);

const DialogsForm = (props) => {
    return <form onSubmit={props.handleSubmit}>
        <div>
            <Field className={myStyle.centerTextArea} name='message' component={message} validate={[required , maxLength15]}/>
            {/* <textarea className={myStyle.centerTextArea} value={props.newMessageText} onChange={changeText} ref={newmessage}></textarea> */}
        </div>
        <div>
            {/* <button className={myStyle.centerButtonSend} onClick={myAlert}>Send</button> */}
            <button className={myStyle.centerButtonSend}>Send</button>
        </div>
    </form>
}

const DialogReduxForm = reduxForm({form: 'messages'}) (DialogsForm)

const Dialogs = (props) =>{
    let updeteDialogues = props.dialogs.map(dialogue => <DialogItem name={dialogue.name} id={dialogue.id}/>)

    let updateMessages = props.messages.map(onemessage => {return letstry(onemessage.text, onemessage.id)})

    //let newmessage = React.createRef();

    let onSubmit = (values) => {
        props.myAlert(values.message);
    }

    return(
        <div className={myStyle.dialogs}>
            <div id={myStyle.avatars} >
                {updeteDialogues}
            </div>
            <div>
                {updateMessages}
            </div>
            <DialogReduxForm onSubmit={onSubmit}/>
            {/* <textarea className={myStyle.centerTextArea} value={props.newMessageText} onChange={changeText} ref={newmessage}></textarea>
            <button className={myStyle.centerButtonSend} onClick={myAlert}>Send</button> */}
        </div>
    )
}

export default Dialogs