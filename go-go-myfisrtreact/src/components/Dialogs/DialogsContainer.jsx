import { NavLink } from 'react-router-dom'
import myStyle from './Dialogs.module.css'
import React from 'react';
import { addMessageActionCreator, writingNewMessageActionCreator } from '../../myRedux/diologs-reducer';
import Dialogs from './Dialogs';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { withAuthRedirectComponent } from '../../hoc/withAuthRedirect';
import { compose } from 'redux';

// const DialogsContainer = (props) =>{
//     let state = props.store.getState();

//     let myAlert = () => {
//         props.store.dispatch(addMessageActionCreator());
//     }

//     let changeText = (text) => {
//         let action = writingNewMessageActionCreator(text)
//         props.store.dispatch(action);
//     }

//     return(
//         <Dialogs myAlert={myAlert} changeText={changeText}  newMessageText={state.dialogwindow.addNewMessage} dialogs={state.dialogwindow.dialogues} messages={state.dialogwindow.messages}/>
//     )
// }

// let AuthRedirectComponent = withAuthRedirectComponent(Dialogs);


const mapStateToProps = (state) => {
    return{
        dialogs: state.dialogwindow.dialogues,
        messages: state.dialogwindow.messages
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        myAlert: (message) => {
            dispatch(addMessageActionCreator(message));
        },
    }
}

let dialogCompose = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withAuthRedirectComponent
)(Dialogs)


// const DialogsContainer = connect(mapStateToProps, mapDispatchToProps)(AuthRedirectComponent);

export default dialogCompose