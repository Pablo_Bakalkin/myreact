import React from 'react';
import styles from './Users.module.css'
import * as axios from 'axios'
import savePhoto from '../../assets/images/241993457.jpg'
import { NavLink } from 'react-router-dom';
import { followAPI, unfollowAPI } from '../../api/api';
import { follow } from '../../myRedux/users-reducer';

const Users = (props) => {
    {
        let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
    
        let pages = [];
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i);
            if(i === 20) break;
        }
    
        return<div>
        <div>
            {
                pages.map(p => {
                    return <span className={props.currentPage === p && styles.boldSym}
                    onClick={(e) => {props.onPageChanged(p); }}>{p}</span>
                })
            }
        </div>
            {/* <button onClick={this.getUsers}>Get Users</button> */}
        {
            props.users.map(user =>
            <div key={user.id}>
                <NavLink to={"/profile/" + user.id}>
                    <img src={user.photos.small != null ? user.photos.small : savePhoto} alt=""/>
                </NavLink>
                <span>
                    <span>{user.name}</span>
                    <span>{user.status}</span>
                </span>
                <span>
                    <span>{'user.location.city'}</span>
                    <span>{'user.location.country'}</span>
                </span>
                <div>
                    {user.followed 
                    ? <button disabled={props.followingInProgress.some(id => id === user.id)} onClick={()=>{
                        props.unfollow(user.id)
                        // props.toggleIsFollowingProgress(true, user.id);
                        // unfollowAPI(user.id)
                        // .then(data => {
                        //     if(data.resultCode == 0){
                        //         props.unfollow(user.id)
                        //     }
                        //     props.toggleIsFollowingProgress(false, user.id);
                        // })

                    }}>Unfollow</button> 
                    : <button disabled={props.followingInProgress.some(id => id === user.id)} onClick={()=>{
                        props.follow(user.id)
                        // props.toggleIsFollowingProgress(true, user.id);
                        // followAPI(user.id)
                        // .then(data => {
                        //     if(data.resultCode == 0){
                        //         props.follow(user.id)
                        //     }
                        //     props.toggleIsFollowingProgress(false, user.id);
                        // })
                        
                    }}>Follow</button>}
                </div>
            </div>
        )
        }
        </div>
    }
}

export default Users;


