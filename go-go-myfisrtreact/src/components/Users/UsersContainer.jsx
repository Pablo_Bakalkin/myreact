import React from 'react';
import { connect } from 'react-redux';
import { checkTotalUsersCount, follow, setCurrentPage, setUsers, toggleIsFetching, unfollow, toggleIsFollowingProgress, requestUsers} from '../../myRedux/users-reducer';
import Users from './Users';
import Preloader from '../Common/Preloader/Preloader';
import { compose } from 'redux';
import { withAuthRedirectComponent } from '../../hoc/withAuthRedirect';
import { getUsers, getCurrentPage, getFollowingInProgress, getIsFetching, getPageSize, getTotalUsersCount } from '../../myRedux/users-selectors';


class UsersAPIComponent extends React.Component{

    componentDidMount() {
        // if(this.props.users.length === 0){
            this.props.requestUsers(this.props.currentPage, this.props.pageSize);
            // this.props.toggleIsFetching(true);
            // getUsersAPI(this.props.currentPage, this.props.pageSize)
            // .then(data => {
            //     this.props.toggleIsFetching(false);
            //     this.props.setUsers(data.items);
            //     this.props.checkTotalUsersCount(data.totalCount)
            // })
        // }
    }

    onPageChanged = (pageNumber) => {
            this.props.setCurrentPage(pageNumber);
            this.props.requestUsers(pageNumber, this.props.pageSize);
            // this.props.toggleIsFetching(true);
            // getUsersAPI(this.props.currentPage, this.props.pageSize)
            // .then(data => {
            //     this.props.toggleIsFetching(false);
            //     this.props.setUsers(data.items);
            // })
    }

    render(){
        return<> 
        {this.props.isFetching ? <Preloader /> : null}
        <Users 
            totalUsersCount={this.props.totalUsersCount}
            pageSize={this.props.pageSize}
            currentPage={this.props.currentPage}
            users={this.props.users}
            unfollow={this.props.unfollow}
            follow={this.props.follow}
            onPageChanged={this.onPageChanged}
            followingInProgress={this.props.followingInProgress}
        ></Users>
        </>
    }
}

let AuthRedirectComponent = withAuthRedirectComponent(UsersAPIComponent);

// const mapStateToProps = (state) => {
//     return{
//         users: state.usersPage.users,
//         pageSize: state.usersPage.pageSize,
//         totalUsersCount: state.usersPage.totalUsersCount,
//         currentPage: state.usersPage.currentPage,
//         isFetching: state.usersPage.isFetching,
//         followingInProgress: state.usersPage.followingInProgress
//     }
// }

const mapStateToProps = (state) => {
    return{
        users: getUsers(state),
        pageSize: getPageSize(state),
        totalUsersCount: getTotalUsersCount(state),
        currentPage: getCurrentPage(state),
        isFetching: getIsFetching(state),
        followingInProgress: getFollowingInProgress(state)
    }
}

// const mapDispatchToProps = (dispatch) => {
//     return{
//         follow: (id) => {
//             dispatch(followAC(id));
//         },
//         unfollow: (id) => {
//             dispatch(unfollowAC(id));
//         },
//         setUsers: (users) => {
//             dispatch(setUsersAC(users));
//         },
//         setCurrentPage: (page) => {
//             dispatch(setCurrentPageAC(page));
//         },
//         checkTotalUsersCount: (totalCount) => {
//             dispatch(checkTotalUsersCountAC(totalCount));
//         },
//         toggleIsFetching: (isFetching) =>{
//             dispatch(toggleIsFetchingAC(isFetching));
//         }
//     }
// }

//const UsersContainer = connect(mapStateToProps, {follow, unfollow, setCurrentPage, getUsers})(AuthRedirectComponent);

export default compose(
    connect(mapStateToProps, {follow, unfollow, setCurrentPage, requestUsers}),
    withAuthRedirectComponent
)(UsersAPIComponent);;