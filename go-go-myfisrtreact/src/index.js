import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './myRedux/redux-store'
import { Provider } from 'react-redux';


//let renderEntireTree = () =>{
    ReactDOM.render(
        <React.StrictMode>
            <Provider store={store}>
                <App />
            </Provider>   {/*<App store={state} dispatch={store.dispatch.bind(store)} store={store}/> */}
        </React.StrictMode>,
        document.getElementById('root')
    );
//}

//  renderEntireTree();   //renderEntireTree(store.getState());

// store.subscribe(() => {
//     // let state = store.getState();
//     // renderEntireTree(state);
//     renderEntireTree();
// });



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
