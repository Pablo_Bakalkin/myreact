import React from 'react';

export const required = (value) => {
    if(value) return undefined

    return 'Required field'
}

export const maxLengthCreator = (max) => {
    return (value) => {
        if(value.length > max) return 'max value symbols';

        return undefined;
    }
}