import * as axios from 'axios'
import { getStatus } from '../myRedux/profile-reducer'

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': '6bd51d65-3d10-4537-8aa0-ffe8c0c8d365'
    }
})

export const getUsersAPI = (currentPage = 1, pageSize = 10) => {
    return instance.get(`users?page=${currentPage}&count=${pageSize}`).then(response => response.data)
}

export const unfollowAPI = (id) => {
    return instance.delete(`follow/${id}`).then(response => response.data)
}

export const followAPI = (id) => {
    return instance.post(`follow/${id}`).then(response => response.data)
}

export const getProfileAPI = (id) => {
    console.warn('Obsolete method. Please profileAPI object')
    return profileAPI.getProfile(id);
    //instance.get(`profile/${id}`).then(response => response.data)
}

export const getLoginAPI = () => {
    console.warn('Obsolete method. Please loginApi object')
    return loginApi.authMe();
    //return instance.get(`auth/me`).then(response => response.data)
}

export const profileAPI = {
    getProfile(userId) {
        return instance.get(`profile/${userId}`).then(response => response.data)
    },

    getStatus(userId) {
        return instance.get(`profile/status/${userId}`);
    },

    updateStatus(status) {
        return instance.put(`profile/status/`, { status: status });
    }
}

export const loginApi = {
    authMe() {
        return instance.get(`auth/me`).then(response => response.data)
    },

    authorizeLogin(myInfo) {
        return instance.post(`auth/login`, {
            email: myInfo.login,
            password: myInfo.password,
            rememberMe: myInfo.rememberMe,
            captcha: null
        })
    },

    authorizeLogout() {
        return instance.delete(`auth/login`)//.then(response => response.data)
    }
}