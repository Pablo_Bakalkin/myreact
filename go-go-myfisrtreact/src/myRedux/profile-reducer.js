import { getProfileAPI, profileAPI } from '../api/api';

const Add_Post = 'Add_Post';
//const Write_New_Post = 'Write_New_Post';
const SET_PROFILE = 'SET_PROFILE'
const SET_STATUS = 'SET_STATUS'

let initialState = {
    //addNewPost:'',

    posts: [
        {likes: '20', text: 'MY man'},
        {likes: '28', text: 'I am a GOD'},
        {likes: '35', text: 'So boring'},
        {likes: '2', text: 'CHiki briki'},
    ],

    profile: null,

    status: ""
}

const profilePostsReducer = (state = initialState, action) => {
    switch (action.type) {
        // case 'Write_New_Post':
        //     return {
        //         ...state,
        //         addNewPost: action.newText
        //     };
        //         // return stateCopy;

        case 'Add_Post':
            let newPost = {
                text: action.posttext,
                likes: 0
            }
            return {
                ...state,
                posts: [...state.posts, newPost],
                addNewPost: ''
            };

        case SET_PROFILE:
            return{
                ...state,
                profile: action.profile
            }

        case SET_STATUS:
            return{
                ...state,
                status: action.status
            }    
        default: 
            return state;
    }
}

// export const writingNewPostActionCreator = (text) => {
//     return { type: Write_New_Post, newText: text};
// }

export const addPostActionCreator = (posttext) => {
    return { type: Add_Post, posttext};
}

export const setProfile = (profile) => {
    return { type: SET_PROFILE, profile};
}

export const setStatus = (status) => {
    return { type: SET_STATUS, status: status}
}

export const getProfile = (userId) => {
    return async(dispatch) => {
        let data = await getProfileAPI(userId)
        dispatch(setProfile(data));
    }
}

export const getStatus = (userId) => {
    return async(dispatch) => {
        let response = await profileAPI.getStatus(userId)
        dispatch(setStatus(response.data));
    }
}

export const updateStatus = (status) => {
    return async(dispatch) => {
        let response = await profileAPI.updateStatus(status)
        if(response.data.resultCode === 0){
                dispatch(setStatus(status));
        }
    }
}

export default profilePostsReducer;