import { combineReducers, createStore, applyMiddleware } from "redux";
import authReducer, { setLogin } from "./auth-reducer";
import dialogsReducer from "./diologs-reducer";
import profilePostsReducer from "./profile-reducer";
import usersReducer from "./users-reducer";
import thunkMiddleware from "redux-thunk";
import { reducer as formReducer } from 'redux-form'
import appReducer from "./app-reducer";

let reducers = combineReducers({
    dialogwindow: dialogsReducer,
    myposts: profilePostsReducer,
    usersPage: usersReducer,
    auth: authReducer,
    app: appReducer,
    form: formReducer
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

window.store = store;

export default store;