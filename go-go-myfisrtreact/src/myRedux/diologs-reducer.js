const Add_Message = 'Add_Message';

let initialState = {
    dialogues: [
        {id: 0, name: 'ANdrey'},
        {id: 1, name: 'Leha'},
        {id: 2, name: 'Jopa'},
        {id: 3, name: 'Ded'},
        {id: 4, name: 'Biggie'},
    ],

    messages: [
        {id: 0, text: 'My man'},
        {id: 1, text: 'LOLOL'},
        {id: 2, text: 'KEKEKEKEK'},
        {id: 3, text: 'andrey gey'},
        {id: 4, text: '.......'},
        {id: 5, text: 'cfrerrre'},
    ]
}

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'Add_Message':{
            return {
                ...state,
                messages: [...state.messages, {id: 6, text: action.message}]
            };
        }
        default:
            return state;
    }
}

export const addMessageActionCreator = (message) => {
    return { type: Add_Message, message}
}

export default dialogsReducer;