import { act } from "react-dom/test-utils";
import dialogsReducer from "./diologs-reducer";
import profilePostsReducer from "./profile-reducer";

let store = {
  _allInfomation: {
    dialogwindow: {
      addNewMessage: '',

      dialogues: [
        {id: 0, name: 'ANdrey'},
        {id: 1, name: 'Leha'},
        {id: 2, name: 'Jopa'},
        {id: 3, name: 'Ded'},
        {id: 4, name: 'Biggie'},
      ],

      messages: [
        {id: 0, text: 'My man'},
        {id: 1, text: 'LOLOL'},
        {id: 2, text: 'KEKEKEKEK'},
        {id: 3, text: 'andrey gey'},
        {id: 4, text: '.......'},
        {id: 5, text: 'cfrerrre'},
      ]
    },

    myposts: {
      addNewPost:'',

      posts: [
        {likes: '20', text: 'MY man'},
        {likes: '28', text: 'I am a GOD'},
        {likes: '35', text: 'So boring'},
        {likes: '2', text: 'CHiki briki'},
      ]
    },

    avatar: [
      'https://steamuserimages-a.akamaihd.net/ugc/80345569855651747/FA7CEA28A702E4BE68B98ADD28082C3757416315/',
      'https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431'
    ]
  },

  _callSubscriber(){
    alert('LOLOLO');
  },
  
  getAllInfomation(){
    return this._allInfomation
  },

  subscribe(callback){
    this._callSubscriber = callback;
  },

  dispatch(action){
    this._allInfomation.myposts = profilePostsReducer(this._allInfomation.myposts, action);
    this._allInfomation.dialogwindow = dialogsReducer(this._allInfomation.dialogwindow, action);
    this._callSubscriber(this._allInfomation);
  }
}

export default store