import React from 'react'
import profilePostsReducer, { addPostActionCreator } from './profile-reducer';

let initialState = {
    //addNewPost:'',

    posts: [
        {likes: '20', text: 'MY man'},
        {likes: '28', text: 'I am a GOD'},
        {likes: '35', text: 'So boring'},
        {likes: '2', text: 'CHiki briki'},
    ],

    profile: null,

    status: ""
}

test('new post should be added', () => {
    // render(<App />);
    // const linkElement = screen.getByText(/learn react/i);
    // expect(linkElement).toBeInTheDocument();
    let  action = addPostActionCreator('EEEEE');

    let newState = profilePostsReducer(initialState, action);

    expect(newState.posts.length).toBe(5);
});
