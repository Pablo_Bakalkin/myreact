import { stopSubmit } from 'redux-form';
import { getLoginAPI, loginApi } from '../api/api';

const SET_USER_DATA = 'SET_USER_DATA';
const SET_LOGIN = 'SET_LOGIN';

let initialState = {
    id: null,
    email: null,
    login: null,
    isAuth: false,
    password: null,
    rememberMe: false
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.mydata,
            }
        
        case SET_LOGIN:
            return {
                ...state,
                ...action.login,
            }

        default:
            return state;
    }
}

export const setAuthUserData = (id, email, login, isAuth) => {
    return { type: SET_USER_DATA, mydata: {id, email, login, isAuth}}
}

export const setLoginAC = (email, password, rememberMe) =>{
    return { type: SET_LOGIN, login: {email, password, rememberMe}}
}

export const getLogin = () => {
    return (dispatch) => {
        return loginApi.authMe()
        .then(data => {
            if(data.resultCode === 0){
                let {id, email, login} = data.data;
                dispatch(setAuthUserData(id, email, login, true));
            }
        })
    }
}

export const setLogin = (allInfo) => {
    return async (dispatch) => {
        // let action = stopSubmit('login', {login: 'ERRRORRe'})
        // dispatch(action)
        //return
        let response = await loginApi.authorizeLogin(allInfo)
        if(response.data.resultCode === 0){
            dispatch(getLogin());
            // alert('We did it')
            // dispatch(setLoginAC(allInfo.login, allInfo.password, allInfo.rememberMe))
        }else{
            dispatch(stopSubmit('login', {_error: response.data.messages.length > 0 ? response.data.messages[0] : 'POTS'}))
        }
    }
}

export const setLogout = () => {
    return async (dispatch) => {
        let response = await loginApi.authorizeLogout()
        if(response.data.resultCode === 0){
            dispatch(setAuthUserData(null, null, null, false));
            // alert('We did it')
            // dispatch(setLoginAC(allInfo.login, allInfo.password, allInfo.rememberMe))
        }
    }
}


export default authReducer