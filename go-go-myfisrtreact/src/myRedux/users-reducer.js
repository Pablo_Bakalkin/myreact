import { getUsersAPI, followAPI, unfollowAPI } from '../api/api';
import {followUnfollowInArr} from '../utils/objects-helper'

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const CHECK_TOTAL_USERS_COUNT = 'CHECK_TOTAL_USERS_COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';

let initialState = {
    users: [
        // {id: 1, photo: 'https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431', followed: false, fullName: 'Pasha Bakal', status: 'I am a GOD', location: {city: 'Odessa', country: 'Ukraine'}},
        // {id: 2, photo: 'https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431', followed: true, fullName: 'Pasha Bakaluse', status: 'I am a GOD', location: {city: 'Kiev', country: 'Ukraine'}},
        // {id: 3, photo: 'https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431', followed: true, fullName: 'Pasha Bakalchik', status: 'I am a GOD', location: {city: 'NewYork', country: 'USA'}},
        // {id: 4, photo: 'https://stalker-city.at.ua/_ph/11/2/241993457.jpg?1608004431', followed: false, fullName: 'Pasha Pupkin', status: 'I am a GOD', location: {city: 'Paris', country: 'France'}},
    ],
    pageSize: 5,
    totalUsersCount: 0,
    currentPage: 1,
    isFetching: false,
    followingInProgress: []
};

const usersReducer = (state = initialState, action) => {
    let userCopy
    switch (action.type) {
        case FOLLOW:
            // userCopy = {
            //     ...state,
            //     users: followUnfollowInArr(state.users, action.userid, 'id', {followed: true})
            // }
            userCopy = {
                ...state,
                users: state.users.map( user => {
                    if(user.id === action.userId){
                        return {...user, followed: true};
                    }
                    return user;
                })
            }
            return userCopy;

        case UNFOLLOW:
            // userCopy = {
            //     ...state,
            //     users: followUnfollowInArr(state.users, action.userid, 'id', {followed: false})
            // }
            userCopy = {
                ...state,
                users: state.users.map( user => {
                    if(user.id === action.userId){
                        return {...user, followed: false};
                    }
                    return user;
                })
            }
            return userCopy;

        case SET_USERS:
            return{
                ...state,
                users: [...action.users]
            }

        case SET_CURRENT_PAGE:
            return{
                ...state,
                currentPage: action.page
            }

        case CHECK_TOTAL_USERS_COUNT:
            return{
                ...state,
                totalUsersCount: action.totalCount
            }
        
        case TOGGLE_IS_FETCHING: {
            return {
                ...state,
                isFetching: action.isFetching
            }
        }

        case TOGGLE_IS_FOLLOWING_PROGRESS: {
            return {
                ...state,
                followingInProgress: action.isFetching 
                ? [...state.followingInProgress, action.userId] 
                : state.followingInProgress.filter(id => id != action.userId)
            }
        }
        default: 
            return state;
    }
}

export const followSuccess = (userId) => {
    return { type: FOLLOW, userId}
}

export const unfollowSuccess = (userId) => {
    return { type: UNFOLLOW, userId}
}

export const setUsers = (users) => {
    return {type: SET_USERS, users}
}

export const setCurrentPage = (page) => {
    return {type: SET_CURRENT_PAGE, page: page}
}

export const checkTotalUsersCount = (totalCount) => {
    return {type: CHECK_TOTAL_USERS_COUNT, totalCount: totalCount}
}

export const toggleIsFetching = (isFetching) => {
    return {type: TOGGLE_IS_FETCHING, isFetching}
}

export const toggleIsFollowingProgress = (isFetching, userId) => {
    return {type: TOGGLE_IS_FOLLOWING_PROGRESS, isFetching, userId}
}

export const requestUsers = (currentPage, pageSize) => {
    return async (dispatch) => {
        dispatch(toggleIsFetching(true));
        let data = await getUsersAPI(currentPage, pageSize)
        dispatch(toggleIsFetching(false));
        dispatch(setUsers(data.items));
        dispatch(checkTotalUsersCount(data.totalCount));
    }
}

const followUnfollow = async (dispatch, apiMethod, actionCreator, userId) => {
    dispatch(toggleIsFollowingProgress(true, userId));
    let data = await apiMethod(userId);
    if(data.resultCode == 0){
        dispatch(actionCreator(userId));
    }
    dispatch(toggleIsFollowingProgress(false, userId));
}

export const unfollow = (userId) => {
    return (dispatch) => {
        followUnfollow(dispatch, unfollowAPI, unfollowSuccess, userId)
        // dispatch(toggleIsFollowingProgress(true, userId));
        // unfollowAPI(userId)
        // .then(data => {
        //     if(data.resultCode == 0){
        //         dispatch(unfollowSuccess(userId));
        //     }
        //     dispatch(toggleIsFollowingProgress(false, userId));
        // })
    }
}

export const follow = (userId) => {
    return (dispatch) => {
        followUnfollow(dispatch, followAPI, followSuccess, userId)
        // dispatch(toggleIsFollowingProgress(true, userId));
        // followAPI(userId)
        // .then(data => {
        //     if(data.resultCode == 0){
        //         dispatch(followSuccess(userId));
        //     }
        //     dispatch(toggleIsFollowingProgress(false, userId));
        // })
    }
}

export default usersReducer; 