import './App.css';
import Navbar from './components/NavBar/NavBar';
import Profile from './components/Profile/Profile';
import Dialogs from './components/Dialogs/Dialogs';
import { BrowserRouter, Route, withRouter } from 'react-router-dom';
import { addMessage } from './myRedux/store';
import DialogsContainer from './components/Dialogs/DialogsContainer';
import UsersContainer from './components/Users/UsersContainer';
import ProfileContainer from './components/Profile/ProfileContainer';
import HeaderContainer from './components/Header/HeaderContainer';
import Login from './components/Login/Login';
import { connect } from 'react-redux';
import { initializeApp } from './myRedux/app-reducer'
import React from 'react';
import { compose } from 'redux';
import Preloader from './components/Common/Preloader/Preloader';


class App extends React.Component {
  componentDidMount(){
    this.props.initializeApp()
  }

  render(){
    if(!this.props.initialized){
      return <Preloader />
    }
    
    return (
      <BrowserRouter>
        <div className='app-wrapper'>
          <HeaderContainer />
          <Navbar />
          <div className="app-wrapper-content">
            <Route exact path="/dialogs" render={ () => <DialogsContainer store={this.props.store}/>}></Route>
            <Route path="/profile/:userId?" render={ () => <ProfileContainer store={this.props.store}/>}></Route>  
            <Route path="/users" render={ () => <UsersContainer store={this.props.store}/>}></Route>
            <Route path="/login" render={ () => <Login store={this.props.store}/>}></Route>
  
            <Route path="/news"><Dialogs /></Route>
            <Route path="/music"><Dialogs /></Route>
            <Route path="/settings"><Dialogs /></Route>
          </div>
          <footer></footer>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStatteToProps = (state) => {
  return{ 
    initialized: state.app.initialized
  }
}

export default compose(
  connect(mapStatteToProps, {initializeApp})
)(App)
